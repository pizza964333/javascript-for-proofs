
CdFR5G8e6nH5JhvsapkHeHjy2FiUQYwXh1d74evuMd3rGcKGnf


транзакция содержащая передачу прав на токен лота


var транзакция = {
  использоватьСмартКонтракты: [{ id: 1, name: 'аукцион01', hash: '5JhvsapkHeHjy2FiUQYwXh1d74evuMd3rGcKGnifCdFR5G8e6nH' }],
  from: 'd74evuMd3rGcKGnifCdFR5G8e6nH5JhvsapkHeHjy2FiUQYwXh1', // хэш последней транзакции отправителя
  to: последняяТранзакция(публичныйКлюч(переменнаяИзСмартКонтракта(1,'лидер торгов')));
  amount: 1,
  transferUnitType: { kind: 'ТокенERKUKU220', hash: 'Hjy2FiUQYwXh1d74evuMd3rGcKGnifCdFR5G8e6nH5JhvsapkHe' },
  требуетсяПруфыНаСмарты: [1],
  signature: 'CdFR5G8e6nH5JhvsapkHeHjy2FiUQYwXh1d74evuMd3rGcKGnif', // содержит публичный ключ отправителя
}

var транзакция = {
  использоватьСмартКонтракты: [{ id: 1, name: 'краудфаундинг01', hash: '5JhvsapkHeHjy2FiUQYwXh1d74evuMd3rGcKGnifCdFR5G8e6nH' }],
  from: 'd74evuMd3rGcKGnifCdFR5G8e6nH5JhvsapkHeHjy2FiUQYwXh1', // хэш последней транзакции отправителя
  to: последняяТранзакция(публичныйКлюч(переменнаяИзСмартКонтракта(РазмножитьДляКаждого(1,'спонсор'))));
  amount: переменнаяИзСмартКонтракта(1,'количество токенов за спонсорство');
  transferUnitType: { kind: 'ТокенERKUKU220', hash: 'Hjy2FiUQYwXh1d74evuMd3rGcKGnifCdFR5G8e6nH5JhvsapkHe' },
  требуетсяПруфыНаСмарты: [1],
  signature: 'CdFR5G8e6nH5JhvsapkHeHjy2FiUQYwXh1d74evuMd3rGcKGnif', // содержит публичный ключ отправителя
}
