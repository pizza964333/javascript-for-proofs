#!/bin/bash
dirs=`find . -mindepth 1 -maxdepth 1 -type d`
rm -f "sha256hex:"*
for dir in $dirs; do
	pushd $dir
	link=`find . -name '*.js' -exec sha256sum {} \; | sha256sum | awk '{ print $1 }'`
	popd
	ln -s $dir "sha256hex:$link"
done
