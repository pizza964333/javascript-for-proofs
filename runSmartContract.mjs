
import * as vm from 'vm';
import * as fs from 'fs';
import * as overload from 'operator-overloading';

function test001(code)
{
	var ncode = (code.replace(/whiteList\(/g,'whiteList(this, ')).replace(/smartRequire\(/g, 'smartRequire(this, ');
	//var nncode = overload.default(" a + b ");
	//console.log(nncode.toString());
	var script = new vm.Script(ncode);
	var sandbox = {
		whiteLists: [],
		smartRequires: [],
		whiteList: function(a,b) {
			a.whiteLists.push(b);
		},
		smartRequire: function(a,b) {
			a.smartRequires.push(b);
		},
	};
	var context = vm.createContext(sandbox);
	try {
		script.runInContext(context);
	} catch(e) {
		//console.log(e);
	}
	console.log(context);
}

function runSmartContract(code)
{
	var script = new vm.Script(code);
	var context = vm.createContext({});
	var a = runAtomicOperation(script,context,'b');
	var b = runAtomicOperation(script,a,'c');
	return;
}

function runAtomicOperation(script,context,atomName)
{
	console.log('=========================================================================');
	//console.log(context);
	delete context.logicState;
	context.savedState = context.state;
	context.atomicOperation = function(a) {
		var smartContract = a.smartContract;
		a.smartContract = undefined;
		if (a.savedState != undefined) {
			a.state = a.savedState;
		}
		var lgs = {
			valids: [],
			invalids: [],
			enabled: true,
			failValue: undefined,
			succValue: undefined,
			status: 'succ',
		};
		var logicInterface = {
			valid:   function(vv) { if (lgs.enabled) { lgs.valids.push(vv); } },
			invalid: function(iv) { lgs.enabled = false; lgs.invalids.push(iv); lgs.valids = []; lgs.status = 'invalid'; },
			fail:    function(fv) { if (lgs.enabled) { lgs.enabled = false; lgs.failValue = fv; lgs.status = 'fail'; }; throw 'atomic fail'; },
			succ:    function(sv) { if (lgs.enabled) { lgs.succValue = sv; }; throw 'atomic succ'; },
		};
		Object.assign(a, logicInterface);
		var ret;
		try { ret = smartContract.atoms[atomName](a); a.succ('end of atomic'); } catch(e) {
			a.logicState = lgs;
			if        (e == 'atomic fail') {
			} else if (e == 'atomic succ') {
			} else {
				throw e;
			}
		}
		return ret;
	};
	var ret = script.runInContext(context);
	delete context.atomicOperation;
	delete context.valid;
	delete context.invalid;
	delete context.fail;
	delete context.succ;
	delete context.smartContract;
	delete context.savedState;
	console.log(['ret',ret]);
	console.log(context);
	return context;
}

fs.readFile('./testSmartContract003.js', function(error,data) {
	test001(data.toString());
	//runSmartContract(data.toString());
});





