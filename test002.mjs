
import {lock, unlock, valid, invalid, succ, fail, onInvalid, onSuccess, Channel, atomic} from "./logic"

function mcnj(a, b)
{
	if (isNotAtomic(a) || isNotAtomic(b)) throw 'mcnj can be used only for atomic arguments';
	atomic(function() {
		a[3].then(function(sv01) {
			b[3].then(function(sv02) {
				succ(['mcnj',sv01,sv02]);
			},        function(fv02) {
				fail(fv02);
			});
		},        function(fv01) {
			fail(fv01);
		});
	});
	
}

function mdsj(a, b)
{
	if (isNotAtomic(a) || isNotAtomic(b)) throw 'mcnj can be used only for atomic arguments';
	atomic(function() {
		a[3].then(function(sv01) {
			b[3].then(function(sv02) {
				succ(['mdsj', sv01, sv02]);
			},        function(fv02) {
				succ(sv01);
			});
		},        function(fv01) {
			b[3].then(function(sv02) {
				succ(sv02);
			},        function(fv02) {
				fail(['mdsj',fv01,fv02]);
			});
		});
	});
	
}

