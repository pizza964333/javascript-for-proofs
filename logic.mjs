

export var fail      = function() { throw 'global fail';      };
export var valid     = function() { throw 'global valid';     };
export var invalid   = function() { throw 'global invalid';   };
export var succ      = function() { throw 'global succ';      };
export var onInvalid = function() { throw 'global onInvalid'; };
export var onSuccess = function() { throw 'global onSuccess'; };

function passTrigger(tf) { tf(); }
function skipTrigger(tf) { return; }

export function lock()
{
}

export function unlock()
{
}

export function Channel(onAccept = passTrigger, onDiscard = skipTrigger)
{
        this.waitings = [];
        this.offers   = [];
        this.send = function(value) {
                var _this = this;
                if (this.waitings.length == 0) {
                        var a = new Promise(function(resolve,reject) {
                                _this.offers.push([value,resolve]);
                        });
                        a.then(function(b) { b(value); });
                        return a;
                } else {
                        (this.waitings.pop())(value);
                        return Promise.resolve();
                }
        };
        this.recv = function(predicate) {
                var _this = this;
                if (this.offers.length == 0) {
                        var a = new Promise(function(resolve,reject) {
                                _this.waitings.push(resolve);
                        });
                        return a;
                } else {
                        var offer = this.offers.pop();
                        var a = new Promise(function(resolve,reject) {
				onDiscard(reject);
				onAccept(function() { offer[1](resolve); });
                        });
			return { value: offer[0], promise: a, then: function(h) { a.then(h); return offer[0]; } };
                }
        };
}

export function atomic(run)
{
	var _valid   = valid;
	var _invalid = invalid;
	var _succ    = succ;
	var _fail    = fail;
	var _onSuccess = onSuccess;
	var _onInvalid = onInvalid;
	var restore = function() {
		valid = _valid;
		invalid = _invalid;
		succ = _succ;
		fail = _fail
		onSuccess = _onSuccess;
		onInvalid = _onInvalid;
		unlock();
	};
	lock();
	var    onSuccTriggers = [];
	var onInvalidTriggers = [];
	var enabled = true;
	onSuccess  = function(tf) {    onSuccTriggers.push(tf); };
	onInvalid  = function(tf) { onInvalidTriggers.push(tf); };
	valid   = function(vv) { if (enabled) { additiveDisj.send(vv); } };
	invalid = function(iv) { enabled = false; onInvalidTriggers.forEach(function(tf){tf();}); additiveConj.send(iv); };
	var additiveConj   = new Channel();
	var additiveDisj   = new Channel(onSuccess,onInvalid);
	var multiplicative = new Promise(function(resolve,reject) {
		succ    = function(sv) { if (enabled) { onSuccTriggers.forEach(function(tf){tf();}); resolve(sv); }; throw('atomic succ'); };
		fail    = function(fv) { if (enabled) { reject(fv); }; throw('atomic fail'); };

		try { run(); succ('end of atomic run'); } catch(e) {
			if        (e == 'atomic succ') {
				restore();
			} else if (e == 'atomic fail') {
				restore();
			} else {
				restore();
				throw(e);
			}
		}
	});
	return ['Atomic',additiveConj,additiveDisj,multiplicative];
}

export function runAtomicContract(scope, run)
{
	return run.bind(scope)();
}





